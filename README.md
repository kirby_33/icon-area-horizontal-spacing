# Icon Area Horizontal Spacing

## Name
gnome-shell extension: Icon Area Horizontal Spacing.

## Description
Reduce the horizontal spacing between icons in the top-left icon area.
Fork of the "Status Area Horizontal Spacing" extension, but for the LEFT side. 
Thank you to the original author: mathematical.coffee

## License
Open source project.

